# Unicorn time

Unicorn time is an application to verify reporting of your and your employees' working time.

It depends on [Redmine](https://www.redmine.org/) and [HRnest](https://hrnest.io/) services.

## Requirements

The application requires [Docker](https://www.docker.com/), [git](https://git-scm.com/) and [make](https://www.gnu.org/software/make/manual/make.html) installed.

## Installing

Clone content of this repository.

```bash
git clone git@gitlab.com:krzysztof.janda/unicorn-time.git
```

Copy the `.env.example` into `.env` file and to fill with proper values.

Use the `make` command to build container and environment for the application.

```bash
make
```

### Configuration

#### General

By setting `HOURS_PER_DAY` you can change how many hours should be counted per workday, e.g.

```
HOURS_PER_DAY=6.5
```

The above example means that your workday is 6,5 h long.

#### People

People configuration is a JSON array of following objects:

```json
{
    "name": "John Snow",           // Name of employee (First and last name)
    "schedule": 0.5,               // Employee schedule, set <1 if part-time (optional)
    "redmine": {
        "id": "666"                // User's Redmine identifier
    },
    "reminders": [
        {
            "channel": "slack",    // Name of integration
            "member": {
                "id": "UABCD012E"  // User's identifier in integrated service
            }
        }
    ]
}
```

Every registered reminder service can be added to `reminders` section.

## Usage

```bash
docker run --rm -v $(pwd)/.env:/unicorn-time/.env:ro unicorn-time [-h|-s|-m=-1]
```

Flags:  
`-h` - show usage  
`-s` - silent mode - skip sending reminders  
`-m=-1` - set relative month to process  
`-d` - debug mode - show debug messages

Example output:

```
APP [i] Run at 1991-11-17 00:05:00
APP [*] Processing month 1991-10
✅ Margaery Tyrell     110.50 / 110.50 
✅ Brandon Stark       143.00 / 143.00 
⏰ Daenerys Targaryen  100.90 / 110.50 
⭕ Jon Snow              0.00 / 136.50 
✅ Arya Stark          130.00 / 130.00 
✅ Sansa Stark         130.83 / 130.00 ✨
APP [*] Sending reminders
```

## Update

To update the application use `make upgrade`.

```bash
make upgrade
```

## Status icons

There are several icons representing status of reported time.

- ✅ - time is reported perfectly
- ✅ + ✨ - reported time is greater than timesheet (overtime)
- ⭕ - some error occurred and at least one of time values is empty or no time has been reported
- ⏰ - reported time is lower than timesheet
- 🤷 - unexpected behaviour occurred; you should never see this icon

## Reminders

*Unicorn time* can send reminder of unreported time using following integrations:

* [Slack](https://slack.com/) (channel: slack)
* [Google Chat](https://workspace.google.com/intl/pl/products/chat/) (channel: gc)

## Creating new reminder service

To add new reminder service follow the steps:

1. Add new section in `.env` file with name of the integration and all required data. Remember to add toggle switch called `YOUR_INTEGRATION_NAME_ENABLED`, e.g. `SLACK_ENABLED`.
2. Add new Go file in `main` directory with the name of new integration, e.g. `slack.go`. Follow good practises listed below.
3. Register your new integration in `CreateReminderServices` method (`reminder.go`).
4. Add new channel configuration to any person in `.env` file.
5. Remember to copy a template of your configuration to `.env.example` file.

### Good practises

New reminder service should implement `ReminderService` interface, and includes the following:

1. configuration structure called `YourIntegrationNameReminderConfig`, e.g. `SlackReminderConfig`,
2. service structure called `yourIntegrationNameService`, e.g. `slackService`, which stores configuration structure,
3. constructor method which takes configuration stucture and returns service structure.

Also, remember to add an unique keyword to identify messages intended to be send by the integration.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
