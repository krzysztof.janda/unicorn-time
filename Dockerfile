# Base
FROM golang:1.20.5-alpine as base

WORKDIR /src

ENV CGO_ENABLED=0

COPY go.* ./

RUN go mod download

COPY . .

# Build
FROM base as build

RUN go build -o /build/app ./pkg/main

# Tests
FROM base AS unit-test

RUN cd pkg/main && GO_ENV=test go test -v .

# Runner
FROM scratch as runner

WORKDIR /unicorn-time

COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /build/app .
COPY --from=build /src/LICENSE .

ENTRYPOINT ["/unicorn-time/app"]
