# 1.6.0

**Added**

- Multi-department

**Changed**

- Project structure and dependency management system
- Upgrade Go version
- Better logger
- [BREAKING] Fix and change department ID ENV key
- [BREAKING] Add multi-stage build and optimize runtime container

# 1.5.0

**Added**

- Standard notification if error/no time reported
- Password change required message

# 1.4.1

**Added**

- Debug mode

**Changed**

- Fix process of absence calculation

# 1.4.0

**Added**

- Google Chat integration

**Changed**

- Employee list is now get from calendar view

# 1.3.0

**Added**

- Optional "relative month to process" parameter
- Run date/time log

# 1.2.0

**Added**

- Optional employee schedule
- Silent flag

**Changed**

- Change overtime additional icon
- Remove unused report status name
- Simplify Docker configuration

# 1.1.0

**Added**

- New log format

**Changed**

- Fix disappearing cookie problem

# 1.0.0

Initial release
