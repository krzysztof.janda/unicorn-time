package main

import (
	"testing"
	"time"
)

func TestBeginningOfMonth(t *testing.T) {
	expectedBeginningOfTheMonth, err := time.ParseInLocation("2006-01-02", "2021-08-01", time.Local)

	if err != nil {
		t.Errorf("Cannot parse date: %s", err.Error())
	}

	date, err := time.ParseInLocation("2006-01-02", "2021-08-25", time.Local)

	if err != nil {
		t.Errorf("Cannot parse date: %s", err.Error())
	}

	beginningOfMonth := BeginningOfMonth(date)

	if expectedBeginningOfTheMonth != beginningOfMonth {
		t.Errorf("Expected: %s; got: %s", expectedBeginningOfTheMonth, beginningOfMonth)
	}
}

func TestEndOfMonth(t *testing.T) {
	expectedEndOfTheMonth, err := time.ParseInLocation("2006-01-02", "2021-08-31", time.Local)

	if err != nil {
		t.Errorf("Cannot parse date: %s", err.Error())
	}

	date, err := time.ParseInLocation("2006-01-02", "2021-08-25", time.Local)

	if err != nil {
		t.Errorf("Cannot parse date: %s", err.Error())
	}

	endOfMonth := EndOfMonth(date)

	if expectedEndOfTheMonth != endOfMonth {
		t.Errorf("Expected: %s; got: %s", expectedEndOfTheMonth, endOfMonth)
	}
}

func TestPreviousMonth(t *testing.T) {
	expectedBeginnigOfPreviousMonth, err := time.ParseInLocation("2006-01-02", "2021-07-01", time.Local)

	if err != nil {
		t.Errorf("Cannot parse date: %s", err.Error())
	}

	date, err := time.ParseInLocation("2006-01-02", "2021-08-25", time.Local)

	if err != nil {
		t.Errorf("Cannot parse date: %s", err.Error())
	}

	beginnigOfPreviousMonth := GetMonth(date, -1)

	if expectedBeginnigOfPreviousMonth != beginnigOfPreviousMonth {
		t.Errorf("Expected: %s; got: %s", expectedBeginnigOfPreviousMonth, beginnigOfPreviousMonth)
	}
}
