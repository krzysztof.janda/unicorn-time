package main

import (
	"strings"

	"github.com/slack-go/slack"
)

type SlackReminderConfig struct {
	Enabled bool
	Message string
	Webhook string
}

type slackService struct {
	config *SlackReminderConfig
}

func NewSlackReminder(config *SlackReminderConfig) ReminderService {
	return &slackService{
		config: config,
	}
}

func (service *slackService) Send(recipients []Person) error {
	if !service.config.Enabled {
		return nil
	}

	message := service.buildMessage(recipients)

	if message == "" {
		return nil
	}

	return slack.PostWebhook(service.config.Webhook, &slack.WebhookMessage{
		Text: message,
	})
}

func (service *slackService) buildMessage(recipients []Person) string {
	var tagList []string

	for _, recipient := range recipients {
		for _, reminder := range recipient.Reminders {
			if reminder.Channel != "slack" {
				continue
			}

			tagList = append(tagList, "<@"+reminder.Member.Id+">")
		}
	}

	if len(tagList) == 0 {
		return ""
	}

	return strings.Replace(service.config.Message, "__PEOPLE__", strings.Join(tagList, " "), -1)
}
