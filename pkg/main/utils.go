package main

func CheckError(err error, msg string, logger LoggerService) {
	if err != nil {
		logger.Debug(err.Error())
		logger.Error(msg)
	}
}

func ReverseArrayOfStrings(values []string) []string {
	for i, j := 0, len(values)-1; i < j; i, j = i+1, j-1 {
		values[i], values[j] = values[j], values[i]
	}
	return values
}
