package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/mattn/go-redmine"
)

type appFlags struct {
	month  *int
	silent *bool
	debug  *bool
}

var (
	logger      = NewLogger("APP", LevelComment)
	environment = NewEnvironmentService(logger, ".env", "../../.env")
)

func main() {
	var people []Person
	var notifyPeople []Person

	flags := parseFlags()

	logger.Info("Run at " + time.Now().Format("2006-01-02 15:04:05"))

	if *flags.month > 0 {
		logger.Error("Month cannot be in the future. Use value less or equal to 0 for current month.")
	}

	if err := json.Unmarshal([]byte(environment.GetEnv("PEOPLE")), &people); err != nil {
		logger.Error("Invalid syntax of PEOPLE in .env file")
	}

	theMonth := GetMonth(time.Now(), *flags.month)

	logger.Comment("Processing month " + theMonth.Format("2006-01"))

	beginningOfMonth := BeginningOfMonth(theMonth)
	endOfMonth := EndOfMonth(theMonth)
	writer := tabwriter.NewWriter(os.Stdout, 1, 1, 1, ' ', 0)

	redmineApi := NewRedmineApi(redmine.NewClient(environment.GetEnv("REDMINE_HOST"), environment.GetEnv("REDMINE_API_KEY")), logger.Spawn("Redmine"))
	hrnestApi := NewHrnestApi(environment.GetEnv("HRNEST_LOGIN"), environment.GetEnv("HRNEST_PASSWORD"), environment.GetEnv("HOURS_PER_DAY"), logger.Spawn("HRnest"))

	for _, person := range people {
		if person.Schedule <= 0 {
			person.Schedule = 1

			logger.Debug("Using default schedule (1.00) for " + person.Name)
		}

		reportedTime := redmineApi.GetTotalReportedTime(person.Redmine.Id, beginningOfMonth, endOfMonth)
		worktime := hrnestApi.GetUserWorktime(person.Name, theMonth.Year(), int(theMonth.Month()), strings.Split(environment.GetEnv("HRNEST_DEPARTMENT_IDS"), ",")) * person.Schedule
		reportStatus := GetReportStatus(worktime, reportedTime)

		if reportStatus.Notify {
			notifyPeople = append(notifyPeople, person)
		}

		fmt.Fprintf(writer, "%s %s\t\t%.2f / %.2f %s\n", reportStatus.Icon, person.Name, reportedTime, worktime, reportStatus.InfoIcon)
	}

	writer.Flush()

	if !*flags.silent {
		reminderServices := CreateReminderServices(environment)

		logger.Comment("Sending reminders")

		errors := SendReminder(reminderServices, notifyPeople)
		for _, err := range errors {
			logger.Warning(err.Error())
		}
	}
}

func parseFlags() *appFlags {
	silent := flag.Bool("s", false, "Silent mode - skip notifications")
	month := flag.Int("m", -1, "Relative month to process; default is previous month")
	debug := flag.Bool("d", false, "Debug mode - show debug messages")
	flag.Parse()

	if *silent {
		logger.Info("Silent mode - NO notifications will be send")
	}

	if *debug {
		logger.Info("Debug mode - debug messages will be displayed")

		logger.SetLogLevel(LevelDebug)
	}

	return &appFlags{
		month:  month,
		silent: silent,
		debug:  debug,
	}
}
