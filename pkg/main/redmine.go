package main

import (
	"fmt"
	"time"

	"github.com/mattn/go-redmine"
)

type RedmineService struct {
	client *redmine.Client
	logger LoggerService
}

func NewRedmineApi(client *redmine.Client, logger LoggerService) *RedmineService {
	return &RedmineService{
		client: client,
		logger: logger,
	}
}

func (service *RedmineService) GetTotalReportedTime(userId string, startDate time.Time, endDate time.Time) float32 {
	var total float32 = 0
	var offset int = 0
	var limit int = 100

	for {
		timeEntries, err := service.client.TimeEntriesWithFilter(
			*redmine.NewFilter(
				"user_id",
				userId,
				"from",
				startDate.Format("2006-01-02"),
				"to",
				endDate.Format("2006-01-02"),
				"offset",
				fmt.Sprint(offset),
				"limit",
				fmt.Sprint(limit),
			),
		)

		CheckError(err, "Cannot filter time entries", service.logger)

		for _, timeEntry := range timeEntries {
			total += timeEntry.Hours
		}

		if offset = len(timeEntries); offset < limit {
			break
		}
	}

	return total
}
