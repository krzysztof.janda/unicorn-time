package main

type ReminderService interface {
	Send(recipients []Person) error
}

func SendReminder(services []ReminderService, recipients []Person) []error {
	var errors []error

	if len(recipients) == 0 {
		return errors
	}

	for _, service := range services {
		err := service.Send(recipients)

		if err != nil {
			errors = append(errors, err)
		}
	}

	return errors
}

func CreateReminderServices(environment EnvironmentService) []ReminderService {
	return []ReminderService{
		NewSlackReminder(&SlackReminderConfig{
			Enabled: environment.GetEnv("SLACK_ENABLED") == "1",
			Message: environment.GetEnv("SLACK_MESSAGE"),
			Webhook: environment.GetEnv("SLACK_WEBHOOK"),
		}),
		NewGoogleChatReminder(&GoogleChatReminderConfig{
			Enabled: environment.GetEnv("GOOGLE_CHAT_ENABLED") == "1",
			Message: environment.GetEnv("GOOGLE_CHAT_MESSAGE"),
			Webhook: environment.GetEnv("GOOGLE_CHAT_WEBHOOK"),
		}),
	}
}
