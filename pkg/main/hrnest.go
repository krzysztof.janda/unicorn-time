package main

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/anaskhan96/soup"
)

const (
	LOGIN_PAGE_URL    = "https://hrnest.io/Account/LogOn"
	WORKTIME_BASE_URL = "https://hrnest.io/leave-planning"

	PADDING_DAY_CLASS = "empty-element"
	SATURDAY_CLASS    = "weekend"
	SUNDAY_CLASS      = "sunday"
	HOLIDAY_CLASS     = "holiday-background"

	PLAN_ITEM_CLASS    = "plan-item"
	PLAN_LEAVE_CLASS   = "plan-leave"
	PLAN_CONTENT_CLASS = "plan-content"
	PLAN_ROW_CLASS     = "plan-row"

	WORKER_CLASS      = "worker"
	WORKER_LIST_CLASS = "list-worker"

	NEW_PASSWORD_FIELD_ID = "Model_NewPassword"
)

type HrnestService struct {
	username    string
	password    string
	client      *http.Client
	worktime    map[string]float32
	hoursPerDay float32
	logger      LoggerService
}

func NewHrnestApi(username string, password string, hoursPerDay string, logger LoggerService) *HrnestService {
	hours, err := strconv.ParseFloat(hoursPerDay, 32)
	CheckError(err, "Cannot convert hours per day to number", logger)

	return &HrnestService{
		username:    username,
		password:    password,
		client:      &http.Client{Jar: NewCookieJar()},
		worktime:    make(map[string]float32),
		hoursPerDay: float32(hours),
		logger:      logger,
	}
}

func (service *HrnestService) GetUserWorktime(name string, year int, month int, departmentIds []string) float32 {
	worktime, ok := service.worktime[name]

	if ok {
		return worktime
	}

	if result := service.logIn(); !result {
		service.logger.Error("Cannot log in")

	}

	service.loadWorktime(year, month, departmentIds)

	worktime, ok = service.worktime[name]

	if !ok {
		service.logger.Error(fmt.Sprintf("Cannot find worktime for %s", name))
	}

	return worktime
}

func (service *HrnestService) checkPasswordChangeRequired(node *soup.Root) {
	newPasswordField := node.Find("input", "id", NEW_PASSWORD_FIELD_ID)

	if newPasswordField.Error == nil {
		service.logger.Error("Password change required")
	}
}

func (service *HrnestService) loadWorktime(year int, month int, departmentIds []string) {
	response, err := soup.GetWithClient(service.createWorktimeUrl(year, month, departmentIds), service.client)
	CheckError(err, "Cannot load worktime", service.logger)

	document := soup.HTMLParse(response)

	service.checkPasswordChangeRequired(&document)

	workers := service.extractWorkers(&document)

	planContainer := document.Find("div", "class", PLAN_CONTENT_CLASS)
	CheckError(planContainer.Error, "Cannot find container of plan content", service.logger)

	plan := planContainer.FindAll("div", "class", PLAN_ROW_CLASS)

	if len(plan) == 0 {
		service.logger.Error("Cannot find plan")
	}

	days := service.calculateMonthDays(&plan[0])
	holidays := service.calculateWeekendsAndHolidays(&plan[1])

	for index, workerPlan := range plan {
		if index < 1 {
			continue
		}

		absence := service.calculateAbsence(&workerPlan)

		service.worktime[workers[index]] = float32(days-holidays-absence) * service.hoursPerDay

		service.logger.Debug(fmt.Sprintf("%s (worked for %d day(s) of %d)", workers[index], days-holidays-absence, days-holidays))
	}
}

func (service *HrnestService) logIn() bool {
	response, err := soup.PostWithClient(LOGIN_PAGE_URL, "application/x-www-form-urlencoded", service.getLogInFormBody(), service.client)
	CheckError(err, "Cannot send log in request", service.logger)

	service.setCookies()

	document := soup.HTMLParse(response)
	link := document.Find("a", "id", "log-out-link")

	return link.Error == nil
}

func (service *HrnestService) getToken() string {
	response, err := soup.GetWithClient(LOGIN_PAGE_URL, service.client)
	CheckError(err, "Cannot obtain CSRF token", service.logger)

	service.setCookies()

	document := soup.HTMLParse(response)
	token := document.Find("input", "name", "__RequestVerificationToken").Attrs()["value"]

	if token == "" {
		service.logger.Error("Cannot find Hrnest request verification token")
	}

	return token
}

func (service *HrnestService) setCookies() {
	requestUrl, err := url.ParseRequestURI("https://hrnest.io")
	CheckError(err, "Cannot parse own URL", service.logger)

	for _, cookie := range service.client.Jar.Cookies(requestUrl) {
		soup.Cookie(cookie.Name, cookie.Value)
	}
}

func (service *HrnestService) getLogInFormBody() url.Values {
	body := url.Values{}
	body.Add("__RequestVerificationToken", service.getToken())
	body.Add("UserName", service.username)
	body.Add("Password", service.password)

	return body
}

func (service *HrnestService) extractWorkers(node *soup.Root) []string {
	workersContainer := node.Find("div", "class", WORKER_LIST_CLASS)
	CheckError(workersContainer.Error, "Cannot find container of workers", service.logger)

	workers := workersContainer.FindAll("div", "class", WORKER_CLASS)

	if len(workers) == 0 {
		service.logger.Error("Cannot find workers")
	}

	workersNames := make([]string, 0)

	for _, worker := range workers {
		name := strings.Join(ReverseArrayOfStrings(strings.Split(worker.FullText(), " ")), " ")
		workersNames = append(workersNames, name)
	}

	return workersNames
}

func (service *HrnestService) calculateMonthDays(node *soup.Root) int {
	return 31 - len(node.FindAll("div", "class", PADDING_DAY_CLASS))
}

func (service *HrnestService) calculateAbsence(workerPlanContainer *soup.Root) int {
	absence := 0

	for _, workerPlan := range workerPlanContainer.FindAll("div", "class", PLAN_ITEM_CLASS) {
		if strings.TrimSpace(workerPlan.FullText()) != "" && !service.isHolidayOrWeekend(&workerPlan) && !service.isAbsencePlan(&workerPlan) {
			absence += 1
		}
	}

	return absence
}

func (service *HrnestService) calculateWeekendsAndHolidays(node *soup.Root) int {
	return len(node.FindAll("div", "class", SATURDAY_CLASS)) +
		len(node.FindAll("div", "class", SUNDAY_CLASS)) +
		len(node.FindAll("div", "class", HOLIDAY_CLASS))
}

func (service *HrnestService) isHolidayOrWeekend(node *soup.Root) bool {
	classes := node.Attrs()["class"]

	return strings.Contains(classes, SATURDAY_CLASS) || strings.Contains(classes, SUNDAY_CLASS) || strings.Contains(classes, HOLIDAY_CLASS)
}

func (service *HrnestService) isAbsencePlan(node *soup.Root) bool {
	return node.Find("div", "class", PLAN_LEAVE_CLASS).Pointer != nil
}

func (service *HrnestService) createWorktimeUrl(year int, month int, departmentIds []string) string {
	request, err := http.NewRequest("GET", WORKTIME_BASE_URL, nil)
	CheckError(err, "Cannot create worktime URL", service.logger)

	yearStr := strconv.Itoa(year)
	query := request.URL.Query()
	query.Add("y", yearStr)
	query.Add("m", strconv.Itoa(month))

	for _, departmentId := range departmentIds {
		query.Add("DIds", departmentId)
	}

	request.URL.RawQuery = query.Encode()

	return request.URL.String()
}
