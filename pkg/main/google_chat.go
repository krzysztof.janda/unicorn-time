package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strings"
)

type GoogleChatReminderConfig struct {
	Enabled bool
	Message string
	Webhook string
}

type googleChatService struct {
	config *GoogleChatReminderConfig
}

func NewGoogleChatReminder(config *GoogleChatReminderConfig) ReminderService {
	return &googleChatService{
		config: config,
	}
}

func (service *googleChatService) Send(recipients []Person) error {
	if !service.config.Enabled {
		return nil
	}

	message := service.buildMessage(recipients)

	if message == "" {
		return nil
	}

	return service.sendPostRequest(message)
}

func (service *googleChatService) buildMessage(recipients []Person) string {
	var tagList []string

	for _, recipient := range recipients {
		for _, reminder := range recipient.Reminders {
			if reminder.Channel != "gc" {
				continue
			}

			tagList = append(tagList, "<users/"+reminder.Member.Id+">")
		}
	}

	if len(tagList) == 0 {
		return ""
	}

	return strings.Replace(service.config.Message, "__PEOPLE__", strings.Join(tagList, " "), -1)
}

func (service *googleChatService) sendPostRequest(message string) error {
	body, _ := json.Marshal(map[string]string{"text": message})
	postBody := bytes.NewBuffer(body)

	resp, err := http.Post(service.config.Webhook, "application/json; charset=UTF-8", postBody)

	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusOK {
		return nil
	}

	defer resp.Body.Close()

	body, err = io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	return errors.New(string(body))
}
