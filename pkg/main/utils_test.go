package main

import "testing"

func TestReverseArrayOfStrings(t *testing.T) {
	array := []string{"str1", "str2", "str3"}
	reversed := []string{"str3", "str2", "str1"}

	got := ReverseArrayOfStrings(array)

	if len(array) != len(got) {
		t.Error("Length of arrays is not equal")
	}

	for i := range array {
		if reversed[i] != got[i] {
			t.Errorf("Expected: %s; got: %s", reversed[i], got[i])
		}
	}
}
