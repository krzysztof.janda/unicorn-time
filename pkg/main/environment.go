package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

type EnvironmentService interface {
	GetEnv(key string) string
}

type environmentService struct {
	filename string
}

func NewEnvironmentService(logger LoggerService, filenames ...string) EnvironmentService {
	for _, filename := range filenames {
		if os.Getenv("GO_ENV") == "test" {
			filename = filename + ".example"
		}

		err := godotenv.Load(filename)

		if err != nil {
			logger.Debug(fmt.Sprintf("Error loading %s file", filename))
			continue
		}

		return &environmentService{
			filename: filename,
		}
	}

	logger.Error("No .env file found")

	return &environmentService{}
}

func (environmentService *environmentService) GetEnv(key string) string {
	return os.Getenv(key)
}
