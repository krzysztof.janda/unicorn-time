package main

import "math"

type ReportStatus struct {
	Name     string
	Icon     string
	InfoIcon string
	Notify   bool
}

func GetReportStatus(worktime float32, reportedTime float32) *ReportStatus {
	worktimeRounded := math.Round(float64(worktime)*100) / 100
	reportedTimeRounded := math.Round(float64(reportedTime)*100) / 100

	// Some error or no reported time
	if worktimeRounded <= 0 || reportedTimeRounded <= 0 {
		return &ReportStatus{
			Icon:   "⭕",
			Notify: true,
		}
	}

	// OK
	if worktimeRounded == reportedTimeRounded {
		return &ReportStatus{
			Icon:   "✅",
			Notify: false,
		}
	}

	// Overtime
	if worktimeRounded < reportedTimeRounded {
		return &ReportStatus{
			Icon:     "✅",
			InfoIcon: "✨",
			Notify:   false,
		}
	}

	// Missing time
	if worktimeRounded > reportedTimeRounded {
		return &ReportStatus{
			Icon:   "⏰",
			Notify: true,
		}
	}

	// Unexpected and unreachable behaviuor
	return &ReportStatus{
		Icon:   "🤷",
		Notify: false,
	}
}
