package main

import (
	"net/http"
	"net/url"
)

type cookieJar struct {
	jar map[string]map[string]*http.Cookie
}

func NewCookieJar() http.CookieJar {
	return &cookieJar{
		jar: make(map[string]map[string]*http.Cookie),
	}
}

func (jar *cookieJar) SetCookies(u *url.URL, cookies []*http.Cookie) {
	for _, cookie := range cookies {
		jar.addCookie(u, cookie)
	}
}

func (jar *cookieJar) addCookie(u *url.URL, c *http.Cookie) {
	if _, ok := jar.jar[u.Host]; !ok {
		jar.jar[u.Host] = make(map[string]*http.Cookie)
	}

	jar.jar[u.Host][c.Name] = c
}

func (jar *cookieJar) Cookies(u *url.URL) []*http.Cookie {
	var cookies []*http.Cookie

	for _, value := range jar.jar[u.Host] {
		cookies = append(cookies, value)
	}

	return cookies
}
