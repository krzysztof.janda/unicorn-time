package main

import "testing"

type (
	dataProvider struct {
		in struct {
			worktime     float32
			reportedTime float32
		}

		out struct {
			name   string
			notify bool
		}
	}

	in struct {
		worktime     float32
		reportedTime float32
	}

	out struct {
		name   string
		notify bool
	}
)

var reportStatusTests = []dataProvider{
	{
		in{worktime: 123.5, reportedTime: 123.5},
		out{name: "OK", notify: false},
	},
	{
		in{worktime: 123.5, reportedTime: 124.5},
		out{name: "OVERTIME", notify: false},
	},
	{
		in{worktime: 0, reportedTime: 123.5},
		out{name: "ERROR", notify: true},
	},
	{
		in{worktime: 123.5, reportedTime: 0},
		out{name: "ERROR", notify: true},
	},
	{
		in{worktime: -1, reportedTime: 123.5},
		out{name: "ERROR", notify: true},
	},
	{
		in{worktime: 123.5, reportedTime: -1},
		out{name: "ERROR", notify: true},
	},
	{
		in{worktime: 123.5, reportedTime: 10},
		out{name: "REPORT", notify: true},
	},
}

func TestReportStatus(t *testing.T) {
	for _, tt := range reportStatusTests {
		t.Run(tt.out.name, func(t *testing.T) {
			reportStatus := GetReportStatus(tt.in.worktime, tt.in.reportedTime)

			if reportStatus.Notify != tt.out.notify {
				t.Errorf("Worktime: %.2f; reported: %.2f; expected: %t; got: %t", tt.in.worktime, tt.in.reportedTime, tt.out.notify, reportStatus.Notify)
			}
		})
	}
}
