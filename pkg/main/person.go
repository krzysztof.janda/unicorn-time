package main

type Person struct {
	Name     string
	Schedule float32
	Redmine  struct {
		Id string
	}
	Reminders []struct {
		Channel string
		Member  struct {
			Id string
		}
	}
}
