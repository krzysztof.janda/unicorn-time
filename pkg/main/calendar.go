package main

import "time"

func BeginningOfMonth(date time.Time) time.Time {
	return time.Date(date.Year(), date.Month(), 1, 0, 0, 0, 0, time.Local)
}

func EndOfMonth(date time.Time) time.Time {
	return BeginningOfMonth(date).AddDate(0, 1, -1)
}

func GetMonth(date time.Time, monthRelative int) time.Time {
	return BeginningOfMonth(date).AddDate(0, monthRelative, 0)
}
