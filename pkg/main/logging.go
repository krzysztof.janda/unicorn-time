package main

import (
	"log"
	"os"
)

type LogLevel int8

const (
	LevelDebug LogLevel = iota
	LevelComment
	LevelInfo
	LevelWarning
	LevelError
)

type LoggerService interface {
	Spawn(pfx string) LoggerService
	Debug(msg string)
	Comment(msg string)
	Info(msg string)
	Warning(msg string)
	Error(msg string)
	SetLogLevel(lvl LogLevel)
}

type loggerService struct {
	logger *log.Logger
	level  LogLevel
}

func NewLogger(prefix string, level LogLevel) LoggerService {
	return &loggerService{
		logger: log.New(os.Stderr, prefix+" ", 0),
		level:  level,
	}
}

func (service *loggerService) Spawn(prefix string) LoggerService {
	return NewLogger(prefix, service.level)
}

func (service *loggerService) Debug(msg string) {
	if service.level > LevelDebug {
		return
	}

	service.logger.Printf("[DEBUG] %s\n", msg)
}

func (service *loggerService) Comment(msg string) {
	if service.level > LevelComment {
		return
	}

	service.logger.Printf("[*] %s\n", msg)
}

func (service *loggerService) Info(msg string) {
	if service.level > LevelInfo {
		return
	}

	service.logger.Printf("[i] %s\n", msg)
}

func (service *loggerService) Warning(msg string) {
	if service.level > LevelWarning {
		return
	}

	service.logger.Printf("[!] %s\n", msg)
}

func (service *loggerService) Error(msg string) {
	if service.level > LevelError {
		return
	}

	service.logger.Fatalf("[-] %s\n", msg)
}

func (service *loggerService) SetLogLevel(level LogLevel) {
	service.level = level
}
