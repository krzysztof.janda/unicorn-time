module krzysztof.janda/unicorn_time

go 1.20

require (
	github.com/anaskhan96/soup v1.2.4
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-redmine v0.0.3
	github.com/slack-go/slack v0.9.4
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/text v0.3.7 // indirect
)
