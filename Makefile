all: load-latest-tag build-image
upgrade: remove-image load-latest-tag build-image
test: unit-test

PLATFORM=`uname -m`

.PHONY: remove-image
remove-image:
	@docker rmi unicorn-time

.PHONY: load-latest-tag
load-latest-tag:
	@git fetch --tags
	$(eval LATEST_TAG_COMMIT=`git rev-list --tags --max-count=1`)
	@git checkout ${LATEST_TAG_COMMIT}

.PHONY: build-image
build-image:
	@docker build -t unicorn-time . --target runner --platform ${PLATFORM}

.PHONY: unit-test
unit-test:
	@docker build . --target unit-test